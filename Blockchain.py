import hashlib as hasher
import json
import datetime as date
from time import time
from flask import Flask, render_template
from flask import request, jsonify
import requests
from textwrap import dedent
from uuid import uuid4
from urllib.parse import urlparse
node = Flask(__name__)

class Block:
    def __init__(self, index, timestamp, data, previous_hash):
        self.index = index
        self.timestamp = timestamp
        self.data = data
        self.previous_hash = previous_hash
        self.hash = self.hashBlock()
        self.size = 5

    #create a sha256 hasher
    #feed it the block contents as strings, then digest them into the block's hash 
    def hashBlock(self):
        sha = hasher.sha256()
        contents = (str(self.index) +
                   str(self.timestamp) +
                   str(self.data) +
                   str(self.previous_hash))
        sha.update(contents.encode('utf-8'))
        return sha.hexdigest()

    #separately create an initial block without a previous hash
def createGenesisBlock(number):
       return Block(0, date.datetime.now(), "Genesis Block", "0")

#Create the chain as a list with the genesis block
blockChain = [createGenesisBlock(0)]
lastBlock = blockChain[0]
#nextBlock contains the data to go in the next block
nextBlock = ""
#the number of strings input for this block
dataCount = 0
#nodes in the network
nodes = set()

#generate a new block based off the previous block and add it to the chain
def newBlock(data):
    global lastBlock 
    newIndex = lastBlock.index + 1
    newTimestamp = date.datetime.now()
    newData = data
    newHash = lastBlock.hash
    freshBlock = Block(newIndex, newTimestamp, newData, newHash)
    blockChain.append(freshBlock)    
    lastBlock = blockChain[-1]
    print("Block #{} has been added to the chain".format(lastBlock.index))
    print("Hash: {}".format(lastBlock.hash))
    print("Chain length is {}\n".format(len(blockChain)))

def addData(data):
    global nextBlock
    global dataCount
    print("adding data: {}".format(data))
    nextBlock = nextBlock + " " + data
    dataCount+=1
    if dataCount >= 5:
        dataCount = 0
        newBlock(nextBlock)
        nextBlock = ""

def registerNode(address):
    parsedURL = urlparse(address)
    nodes.add(parsedURL.netloc)

def checkValidChain(chain):
    prevBlock = chain[0]
    current = 1

    while current < len(chain):
        block = chain[current]
        #print(f'{prevBlock}')
        print("{}".format(prevBlock))

        #check hashes match
        if prevBlock['hash'] != block['previous_hash']:
            return False

        prevBlock = block
        current += 1

    return True


def chainConflict():
    neighbours = nodes
    new_chain = None
    global blockChain

    max_length = len(blockChain)

    for node in neighbours:
        #response = requests.get(f'http://{node}/chain')
        response = requests.get('http://{}/chain'.format(node))

        if response.status_code == 200:
            length = len(response)
            chain = response

            if length > max_length and checkValidChain(chain):
                max_length = length
                new_chain = chain

    if new_chain:
        blockChain = toChain(new_chain)
        return True

    return False

#adds 10 dummy data entries to populate the chain
for i in range(0, 10):
    addData("Dummy entry number {}".format(i))

#convert from json to blockChain        
def toChain(chainIn):
    newChain = []
    for block in (chainIn):
        newBlock.index = block['index']
        newBlock.data = block['data']
        newBlock.hash = block['hash']
        newBlock.timestamp = block['timestamp']
        newBlock.previous_hash = block['previous_hash']
        newBlock.size = block['size']
        newChain.append(newBlock)
    return newChain

node_identifier = str(uuid4()).replace('-', '')
#print(f"This node is called: {node_identifier}\n")
print("This node is called: {}\n".format(node_identifier))

#write the blockChain to json format
def convertChain():
    converted = []
    for i in blockChain:
        block = {"index" : i.index, "timestamp" : i.timestamp, "data" : i.data, "previous_hash" : i.previous_hash, "hash" : i.hash, "size" : i.size}
        converted.append(block)
    return converted

#get the full blockchain 
@node.route('/chain', methods=['GET'])
def getChain():
    textChain = convertChain()
    response = { 'chain': textChain }
    #print(jsonify(response))
    return jsonify(response), 200

#write user input data to the blockchain
@node.route('/write', methods=['POST'])
def writeData():
    values = request.get_json(force=True)
    addData(values['data'])

    return jsonify({'message' : "data added to the chain"}), 201

@node.route('/nodes/register', methods=['POST'])
def registerNodes():
    values = request.get_json()
    global nodes
    inNodes = values.get('nodes')
    if inNodes is None:
        return "Error: please supply a list of nodes", 400

    for node in inNodes:
        registerNode(node)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(nodes)
    }
    return jsonify(response), 201

@node.route('/nodes/resolve', methods=['GET'])
def consensus():
    replaced = chainConflict()

    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': convertChain()
        }
    else:
        response = {
            'message' : 'Our chain is master',
            'new_chain': convertChain()
        }

    return jsonify(response), 200

if __name__ == '__main__':
    node.run()
